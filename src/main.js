var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var sample
var imgSize = 0.7
var rand = {
  x: null,
  y: null
}

function preload() {
  sample = loadImage('img/fortepan_190481.jpg')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  rand.x = Math.floor(Math.random() * sample.width)
  rand.y = Math.floor(Math.random() * sample.height)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  image(sample, windowWidth * 0.5 - boardSize * imgSize * 0.5, windowHeight * 0.5 - boardSize * imgSize * 0.5, boardSize * imgSize, boardSize * imgSize)

  fill(0)
  noStroke()
  ellipse(windowWidth * 0.5 - boardSize * imgSize * 0.5 + (rand.x / sample.width) * boardSize * imgSize, windowHeight * 0.5 - boardSize * imgSize * 0.5 + (rand.y / sample.height) * boardSize * imgSize, boardSize * 0.1, boardSize * 0.1)

  stroke(sample.get(rand.x, rand.y))
  noFill()
  strokeWeight(boardSize * 0.125)
  strokeCap(PROJECT)
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize * 0.775, boardSize * 0.775)

  rand.x = Math.floor(sin(frameCount * (0.01 + 0.01 * (mouseX / windowWidth))) * sample.width * 0.35 + sample.width * 0.5)
  rand.y = Math.floor(cos(frameCount * (0.01 + 0.01 * (mouseY / windowHeight))) * sample.height * 0.35 + sample.height * 0.5)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
